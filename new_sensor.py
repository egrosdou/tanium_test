sample_sensor = {
   'category':'Tanium Diagnostics',
   'content_set': {
      'id':100,
      'name':'Default'
   },
   'delimiter':'|',
   'description':'Eleni sample sensor',
   'exclude_from_parse_flag':True,
   'ignore_case_flag':True,
   'keep_duplicates_flag':False,
   'max_age_seconds':300,
   'max_string_age_minutes':900,
   'max_strings': 100000,
   'name':'Eleni\'s sample sensor',
   'parameter_definition':'',
   'queries':[
      {
         'platform': 'Windows',
         'script':'Reserved',
         'script_type':'Python'
      }
   ],
   'source_id':0,
   'string_count':0,
   'value_type':'String'
}