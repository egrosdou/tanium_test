#! /urs/bin/env python
import requests
import json
import tan_credentials
import new_question
import new_sensor
import add_user

#initiate the authentication
def initiate_call(url):
    try:
        #create header for the HTTP request
        header = {
            'content-type':'application/json'
        }
        #create a response
        response = requests.post(url+'session/login', headers=header, data=json.dumps(tan_credentials.credentials), verify=False).json()
        #print(json.dumps(response, indent=2, sort_keys=True))

        #get the session ID provided and create a new API token
        session_id = response['data']['session']
        #print(session_id)
        return session_id
    except requests.exceptions.RequestException as e:
        print('Seems like the server does not respond at this point..')
        raise SystemExit(e)


#get the token of the API call
def get_token(url):
    headers = {
        'session':initiate_call(url)
    }

    response_api = requests.post(url+'api_tokens', headers=headers, data="{}", verify=False).json()
    print(json.dumps(response_api, indent=2, sort_keys=True))

    #get the token and start playing around
    token = response_api['data']['token_string']
    #print(token)
    return headers

#get the server information
def get_server_info(url):
    #retrieve the server info
    response_server_info = requests.get(url+'server_info', headers=get_token(url), verify=False).json()
    print('###### Server Information ######' + json.dumps(response_server_info, indent=2, sort_keys=True))

#get the client information
def get_client_info(url):
    response_client_count = requests.get(url+'client_count', headers=get_token(url), verify=False).json()
    print(json.dumps(response_client_count, indent=2, sort_keys=True))

#get the license file and license information
def get_license_info(url):
    response_license_file = requests.get(url+'license',headers=get_token(url), verify=False).json()
    print('The name of the license file is: '+json.dumps(response_license_file, indent=2, sort_keys=True))

#create a new user in the Tanium Server
def create_user(url):
    response_create_user = requests.post(url+'users', headers=get_token(url), data=json.dumps(add_user.add_user), verify=False).json()
    print('A new user was created: '+json.dumps(response_create_user, indent=2, sort_keys=True))

#create new question to particular endpoints
def create_question(url):
    response_create_question = requests.post(url+'questions', headers=get_token(url), data=json.dumps(new_question.question_OS), verify=False).json()
    print(json.dumps(response_create_question, indent=2, sort_keys=True))
    return response_create_question['data']['id']

#save the question created above
def save_question(url):
    saved_question = {
        'name':'Eleni Test saved Question',
        'question': {
            'id':create_question(url)
        }
    }
    response_save_question = requests.post(url+'saved_questions', headers=get_token(url), data=json.dumps(saved_question), verify=False).json()
    print(json.dumps(response_save_question, indent=2, sort_keys=True))

#create a new test sensor
def create_new_sensor(url):
    try:
        response_create_sensor = requests.post(url+'sensors', headers=get_token(url), data=json.dumps(new_sensor.sample_sensor), verify=False).json()
        print(json.dumps(response_create_sensor, indent=2, sort_keys=True))
    except requests.exceptions.BaseHTTPError as e:
        print('It seems like you are not able to create a sensor at this point. Ty again later!')
        raise SystemExit(e)

if __name__ == '__main__':
    #define the base URL
    url = 'https://WIN-J9T0MEOHBGB/api/v2/'
    while True:
        print('Click one of the following options; 1. Authenticate yourself 2. Get server information 3. Get client count 4. Get the license information 5. Create a new user 6. Create a new question 7.Create a new sensor 8. EXIT')
        get_input = input("Provide the action you would like to perform: ")
        list = ['1', '2', '3', '4', '5', '6', '7']
        if get_input in list:
            if get_input == '1':
                initiate_call(url)
                get_token(url)
            elif get_input == '2':
                 get_server_info(url)
            elif get_input == '3':
                get_client_info(url)
            elif get_input == '4':
                 get_license_info(url)
            elif get_input == '5':
                create_user(url)
            elif get_input == '6':
                create_question(url)
            elif get_input == '7':
                create_new_sensor(url)
            elif get_input == '8':
                exit()
            else:
                print('Please, provide a valid number and try again.')
        else:
            print('Provide a valid name and try again...')
